package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/jroimartin/gocui"
)

// TODO: link endpoints with sizes and update functions (inside some map maybe)
const (
	apiUrl       = "http://127.0.0.1:5001/api/v0"
	statsBwU     = "/stats/bw"
	swarmPeersU  = "/swarm/peers"
	bitswapStatU = "/bitswap/stat"
	diagSysU     = "/diag/sys"
	idU          = "/id"
)

func init() {

}

func getEndpoint(endpoint string) []byte {
	resp, err := http.Get(apiUrl + endpoint)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	return body
}

//func getDiagSys() []byte {
//	return getEndpoint(diagSys)
//}

func getStatsBw() []byte {
	return getEndpoint(statsBwU)
}

type statsBw struct {
	TotalIn  uint64
	TotalOut uint64
	RateIn   float64
	RateOut  float64
}

func getPeersCount() []byte {
	return getEndpoint(swarmPeersU)
}

type protocol struct {
	Protocol string
}

type peer struct {
	Addr    string
	Peer    string
	Latency string
	Muxer   string
	Streams []protocol
}

type swarmPeers struct {
	Peers []peer
}

func getBitswapStat() []byte {
	return getEndpoint(bitswapStatU)
}

type bitswapStat struct {
	ProvideBufLen   int32
	Wantlist        []string
	Peers           []string
	BlocksReceived  uint64
	DataReceived    uint64
	BlocksSent      uint64
	DataSent        uint64
	DupBlksReceived uint64
	DupDataReceived uint64
}

func getId() []byte {
	return getEndpoint(idU)
}

type id struct {
	ID              string
	PublicKey       string
	Addresses       []string
	AgentVersion    string
	ProtocolVersion string
}

func updateLayoutEvery(d time.Duration, g *gocui.Gui) {
	for range time.Tick(d) {
		go g.Update(func(g *gocui.Gui) error {
			v, _ := g.View("stats-bw")
			v.Clear()
			sb := statsBw{}
			json.Unmarshal(getStatsBw(), &sb)
			fmt.Fprintf(v, "TotalIn: %s\n", humanize.Bytes(sb.TotalIn))
			fmt.Fprintf(v, "TotalOut: %s\n", humanize.Bytes(sb.TotalOut))
			fmt.Fprintf(v, "RateIn: %s/s\n", humanize.Bytes(uint64(sb.RateIn)))
			fmt.Fprintf(v, "RateOut: %s/s\n", humanize.Bytes(uint64(sb.RateOut)))
			return nil
		})
		// TODO: display all the peers with latencies and more info
		// on click
		go g.Update(func(g *gocui.Gui) error {
			v, _ := g.View("peers-count")
			v.Clear()
			sp := swarmPeers{}
			json.Unmarshal(getPeersCount(), &sp)
			fmt.Fprintf(v, "Peers: %d\n", len(sp.Peers))
			return nil
		})
		go g.Update(func(g *gocui.Gui) error {
			v, _ := g.View("bitswap-stat")
			v.Clear()
			bs := bitswapStat{}
			json.Unmarshal(getBitswapStat(), &bs)
			fmt.Fprintf(v, "Blocks received: %d\n", bs.BlocksReceived)
			fmt.Fprintf(v, "Blocks sent: %d\n", bs.BlocksSent)
			fmt.Fprintf(v, "Data sent: %s\n", humanize.Bytes(bs.DataSent))
			fmt.Fprintf(v, "Data received: %s\n", humanize.Bytes(bs.DataReceived))
			return nil
		})
		// TODO: don't update it all the time, it's const during one session
		go g.Update(func(g *gocui.Gui) error {
			v, _ := g.View("id")
			v.Clear()
			id_ := id{}
			json.Unmarshal(getId(), &id_)
			fmt.Fprintf(v, "ID: %s\n", id_.ID)
			fmt.Fprintf(v, "Agent version: %s\n", id_.AgentVersion)
			fmt.Fprintf(v, "Protocol version: %s\n", id_.ProtocolVersion)
			return nil
		})
	}
}

func main() {
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()

	g.SetManagerFunc(layout)

	// TODO: make it configurable through args
	go updateLayoutEvery(time.Second, g)

	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

// TODO: organize these size values (map maybe?)
func layout(g *gocui.Gui) error {
	if v, err := g.SetView("stats-bw", 0, 0, 40, 5); err != nil {
		v.Title = "Bandwith stats"
		if err != gocui.ErrUnknownView {
			return err
		}
	}
	if v, err := g.SetView("peers-count", 41, 0, 80, 5); err != nil {
		v.Title = "Peers count"
		if err != gocui.ErrUnknownView {
			return err
		}
	}
	if v, err := g.SetView("bitswap-stat", 0, 6, 40, 15); err != nil {
		v.Title = "Bitswap stats"
		if err != gocui.ErrUnknownView {
			return err
		}
	}
	if v, err := g.SetView("id", 0, 16, 80, 20); err != nil {
		v.Title = "ID"
		if err != gocui.ErrUnknownView {
			return err
		}
	}
	return nil
}

func quit(_ *gocui.Gui, _ *gocui.View) error {
	return gocui.ErrQuit
}
