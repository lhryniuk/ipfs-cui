# ipfs-cui [![Build Status](https://travis-ci.com/hryniuk/ipfs-cui.svg?branch=master)](https://travis-ci.com/hryniuk/ipfs-cui)

Console interface collecting several diagnostic information.

### Get&run

```shell
$ go get github.com/hryniuk/ipfs-cu
$ ipfs-cui
```


### Screenshot

![screenshot](https://raw.githubusercontent.com/hryniuk/ipfs-cui/master/screenshot.png)
